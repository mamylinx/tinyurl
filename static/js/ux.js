/**
 * Created by mamy on 5/28/20.

 * For a better UX purpose.
 * Click 'copy' button to copy shortened url into clipboard
 **/
const button = document.getElementById("button");
button.onclick = function () {
    document.execCommand("copy");
};
button.addEventListener("copy", function (event) {
    event.preventDefault();
    if (event.clipboardData) {
        event.clipboardData.setData("text/plain", document.getElementById("url").textContent);
        button.innerText = "Copied";
    }
});
