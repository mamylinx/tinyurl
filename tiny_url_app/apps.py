from django.apps import AppConfig


class TinyUriConfig(AppConfig):
    name = 'tiny_url_app'
