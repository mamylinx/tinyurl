# Generated by Django 3.0.6 on 2020-05-29 08:41

from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Uri',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='id')),
                ('long_uri', models.URLField(max_length=255, unique=True)),
            ],
        ),
    ]
