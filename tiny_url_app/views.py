from django.contrib import admin
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.views.decorators.http import require_GET

from tiny_url_app.forms import UriForm
from tiny_url_app.helpers import *
from tiny_url_app.models import Uri


@require_GET
def index(request):
    form = UriForm(request.GET)
    if form.is_valid():
        _uri = form.cleaned_data['long_uri']
        Uri.objects.get_or_create(long_uri=_uri)
        _long_uri = Uri.objects.get(long_uri=_uri)
        tiny_uri = request.scheme + '://' + request.get_host() + '/' + dehydrate(_long_uri.id)
    else:
        form = UriForm()
        tiny_uri = None

    return render(request, "main_tpl.html", {
        'form': form,
        'tiny_uri': tiny_uri
    })


def redirect(request, uri_key):
    if uri_key != admin.site:
        primary_key = saturate(uri_key)
        decoded_uri = get_object_or_404(Uri, id=primary_key)

        return HttpResponseRedirect("%s" % decoded_uri)
