from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<uri_key>', views.redirect, name='redirect')
]
