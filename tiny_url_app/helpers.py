import math

BASE = 62
UPPERCASE_OFFSET = 55
LOWERCASE_OFFSET = 61
DIGIT_OFFSET = 48


def true_order(char):
    """
    :param char: A digit in character representation with base [BASE] 
    :return: integer
    """

    if char.isdigit():
        return ord(char) - DIGIT_OFFSET
    elif 'A' <= char <= 'Z':
        return ord(char) - UPPERCASE_OFFSET
    elif 'a' <= char <= 'z':
        return ord(char) - LOWERCASE_OFFSET
    else:
        raise ValueError("%s is not a valid character" % char)


def true_character(integer):
    """
    :param integer: integer [integer] 
    :return: A digit in character representation with base [BASE]
    """
    if integer < 10:
        return chr(integer + DIGIT_OFFSET)
    elif 10 <= integer <= 35:
        return chr(integer + UPPERCASE_OFFSET)
    elif 36 <= integer < 62:
        return chr(integer + LOWERCASE_OFFSET)
    else:
        raise ValueError("%d is not a valid integer in the range of base %d" % (integer, BASE))


def saturate(key):
    """
    Turn key string into an ID
    :param key: base [BASE] number [key] in string representation
    :return: integer ID
    """
    int_sum = 0
    reversed_key = key[::-1]
    for idx, char in enumerate(reversed_key):
        int_sum += true_order(char) * int(math.pow(BASE, idx))
    return int_sum


def dehydrate(integer):
    """
    Turn an ID into a key string
    :param integer: integer [integer] ID 
    :return: base [BASE] number in string representation
    """
    if integer == 0:
        return '0'

    string = ""
    while integer > 0:
        remainder = integer % BASE
        string = true_character(remainder) + string
        integer = int(integer / BASE)
    return string
