from django import forms


class UriForm(forms.Form):
    long_uri = forms.URLField(label='URL to shorten + press Enter', max_length=200)
