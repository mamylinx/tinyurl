from django.db import models


class Uri(models.Model):
    id = models.AutoField('id', primary_key=True, auto_created=True)
    long_uri = models.URLField(unique=True, max_length=255)

    def __str__(self):
        return self.long_uri

    def __unicode__(self):
        return "%s" % (self.id)
